<?php

namespace App\Entity;

use App\Repository\AuthorRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AuthorRepository::class)]
class Author
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 20)]
    private ?string $openlibraryKey = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $birthDay = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $birthMonth = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $birthYear = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $deathDay = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $deathMonth = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $deathYear = null;

    public function __construct($values = [])
    {
        if (!empty($values)) {
            $this->name = $values['name'];
            $this->openlibraryKey = $values['openlibrary_key'];
            $this->birthDay = $values['birth_day'];
            $this->birthMonth = $values['birth_month'];
            $this->birthYear = $values['birth_year'];
            $this->deathDay = $values['death_day'];
            $this->deathMonth = $values['death_month'];
            $this->deathYear = $values['death_year'];
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOpenlibraryKey(): ?string
    {
        return $this->openlibraryKey;
    }

    public function setOpenlibraryKey(string $openlibraryKey): self
    {
        $this->openlibraryKey = $openlibraryKey;

        return $this;
    }

    public function getBirthDay(): ?int
    {
        return $this->birthDay;
    }

    public function setBirthDay(?int $birthDay): self
    {
        $this->birthDay = $birthDay;

        return $this;
    }

    public function getBirthMonth(): ?int
    {
        return $this->birthMonth;
    }

    public function setBirthMonth(?int $birthMonth): self
    {
        $this->birthMonth = $birthMonth;

        return $this;
    }

    public function getBirthYear(): ?int
    {
        return $this->birthYear;
    }

    public function setBirthYear(?int $birthYear): self
    {
        $this->birthYear = $birthYear;

        return $this;
    }

    public function getDeathDay(): ?int
    {
        return $this->deathDay;
    }

    public function setDeathDay(?int $deathDay): self
    {
        $this->deathDay = $deathDay;

        return $this;
    }

    public function getDeathMonth(): ?int
    {
        return $this->deathMonth;
    }

    public function setDeathMonth(?int $deathMonth): self
    {
        $this->deathMonth = $deathMonth;

        return $this;
    }

    public function getDeathYear(): ?int
    {
        return $this->deathYear;
    }

    public function setDeathYear(?int $deathYear): self
    {
        $this->deathYear = $deathYear;

        return $this;
    }

    public function getBirthDate(): string
    {
        $birthDate = '';
        if (null !== $this->birthYear) {
            $birthDate .= $this->birthYear;
            if (null !== $this->birthMonth) {
                $birthDate .= '-'.str_pad($this->birthMonth, 2, '0', STR_PAD_LEFT);
            }
            if (null !== $this->birthDay) {
                $birthDate .= '-'.str_pad($this->birthDay, 2, '0', STR_PAD_LEFT);
            }
        }

        return $birthDate;
    }

    public function getDeathDate(): string
    {
        $deathDate = '';
        if (null !== $this->deathYear) {
            $deathDate .= $this->deathYear;
            if (null !== $this->deathMonth) {
                $deathDate .= '-'.str_pad($this->deathMonth, 2, '0', STR_PAD_LEFT);
            }
            if (null !== $this->deathDay) {
                $deathDate .= '-'.str_pad($this->deathDay, 2, '0', STR_PAD_LEFT);
            }
        }

        return $deathDate;
    }
}
