<?php

namespace App\EventSubscriber;

use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityDeletedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    private $session;

    public function __construct()
    {
    }

    public function onAfterEntityPersistedEvent(AfterEntityPersistedEvent $event): void
    {
        // ...
    }

    public function onAfterEntityUpdatedEvent(AfterEntityUpdatedEvent $event): void
    {
        // ...
    }

    public function onAfterEntityDeletedEvent(AfterEntityDeletedEvent $event): void
    {
        // ...
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AfterEntityPersistedEvent::class => ['onAfterEntityPersistedEvent'],
            AfterEntityUpdatedEvent::class => ['onAfterEntityUpdatedEvent'],
            AfterEntityDeletedEvent::class => ['onAfterEntityDeletedEvent'],
        ];
    }
}
