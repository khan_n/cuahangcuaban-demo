<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class Author extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        foreach (self::getMockData() as $record) {
            $manager->persist(new \App\Entity\Author($record));
        }
        $manager->flush();
    }

    public static function getMockData(): array
    {
        return [
            [
                'name' => 'Dante Alighieri',
                'openlibrary_key' => 'OL9356937A',
                'birth_year' => 1321,
                'birth_month' => null,
                'birth_day' => null,
                'death_year' => 1265,
                'death_month' => null,
                'death_day' => null,
            ],
            [
                'name' => 'Nhật Ánh Nguyễn',
                'openlibrary_key' => 'OL6997681A',
                'birth_year' => 1985,
                'birth_month' => null,
                'birth_day' => null,
                'death_year' => null,
                'death_month' => null,
                'death_day' => null,
            ],
            [
                'name' => '春樹 村上',
                'openlibrary_key' => 'OL382524A',
                'birth_year' => 1949,
                'birth_month' => 01,
                'birth_day' => 12,
                'death_year' => null,
                'death_month' => null,
                'death_day' => null,
            ],
        ];
    }
}
