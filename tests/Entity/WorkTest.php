<?php

namespace App\Tests\Entity;

use App\Entity\Work;
use PHPUnit\Framework\TestCase;

/**
 * @group Entity
 * @group Entity_Work
 */
class WorkTest extends TestCase
{
    private Work $work;

    protected function setUp(): void
    {
        parent::setUp();
        $this->work = new Work([
            'title'             => "TDivine Comedy: Inferno (Bloom's Reviews: Comprehensive Research) (Bloom's Reviews: 
Comprehensive Research & Study Guides)",
            'subtitle'          => '',
            'openlibrary_key'   => 'OL28602125W',
            'isbn13'            => '9780791041192',
            'description'       => null,
        ]);
    }

    public function testAuthorEntity(): void
    {
        $this->assertEquals("TDivine Comedy: Inferno (Bloom's Reviews: Comprehensive Research) (Bloom's Reviews: 
Comprehensive Research & Study Guides)", $this->work->getTitle());
        $this->assertEquals('', $this->work->getSubtitle());
        $this->assertEquals('OL28602125W', $this->work->getOpenlibraryKey());
        $this->assertEquals('9780791041192', $this->work->getIsbn13());
    }
}
