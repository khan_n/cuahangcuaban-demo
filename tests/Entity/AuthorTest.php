<?php

namespace App\Tests\Entity;

use App\Entity\Author as AuthorEntity;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use PHPUnit\Framework\TestCase;

/**
 * @group Entity
 * @group Entity_Author
 */
class AuthorTest extends TestCase
{
    private $author;

    protected function setUp(): void
    {
        parent::setUp();
        $this->author = new AuthorEntity([
            'name'              => 'Dante Alighieri',
            'openlibrary_key'   => 'OL9356937A',
            'birth_year'        => 1321,
            'birth_month'       => null,
            'birth_day'         => null,
            'death_year'        => 1265,
            'death_month'       => null,
            'death_day'         => null,
        ]);
    }

    public function testAuthorEntity(): void
    {
        $this->assertEquals('Dante Alighieri', $this->author->getName());
        $this->assertEquals('OL9356937A', $this->author->getOpenlibraryKey());
        $this->assertEquals(1321, $this->author->getBirthYear());
        $this->assertEquals(null, $this->author->getBirthMonth());
        $this->assertEquals(null, $this->author->getBirthDay());
        $this->assertEquals(1265, $this->author->getDeathYear());
        $this->assertEquals(null, $this->author->getDeathMonth());
        $this->assertEquals(null, $this->author->getDeathDay());
    }
}
