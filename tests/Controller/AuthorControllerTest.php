<?php

namespace App\Test\Controller;

use App\Entity\Author;
use App\Repository\AuthorRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthorControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private AuthorRepository $repository;
    private string $path = '/author/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = (static::getContainer()->get('doctrine'))->getRepository(Author::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Author index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'author[name]' => 'Testing',
            'author[openlibraryKey]' => 'Testing',
            'author[birthDay]' => 'Testing',
            'author[birthMonth]' => 'Testing',
            'author[birthYear]' => 'Testing',
            'author[deathDay]' => 'Testing',
            'author[deathMonth]' => 'Testing',
            'author[deathYear]' => 'Testing',
        ]);

        self::assertResponseRedirects('/author/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Author();
        $fixture->setName('My Title');
        $fixture->setOpenlibraryKey('My Title');
        $fixture->setBirthDay('My Title');
        $fixture->setBirthMonth('My Title');
        $fixture->setBirthYear('My Title');
        $fixture->setDeathDay('My Title');
        $fixture->setDeathMonth('My Title');
        $fixture->setDeathYear('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Author');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Author();
        $fixture->setName('My Title');
        $fixture->setOpenlibraryKey('My Title');
        $fixture->setBirthDay('My Title');
        $fixture->setBirthMonth('My Title');
        $fixture->setBirthYear('My Title');
        $fixture->setDeathDay('My Title');
        $fixture->setDeathMonth('My Title');
        $fixture->setDeathYear('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'author[name]' => 'Something New',
            'author[openlibraryKey]' => 'Something New',
            'author[birthDay]' => 'Something New',
            'author[birthMonth]' => 'Something New',
            'author[birthYear]' => 'Something New',
            'author[deathDay]' => 'Something New',
            'author[deathMonth]' => 'Something New',
            'author[deathYear]' => 'Something New',
        ]);

        self::assertResponseRedirects('/author/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getName());
        self::assertSame('Something New', $fixture[0]->getOpenlibraryKey());
        self::assertSame('Something New', $fixture[0]->getBirthDay());
        self::assertSame('Something New', $fixture[0]->getBirthMonth());
        self::assertSame('Something New', $fixture[0]->getBirthYear());
        self::assertSame('Something New', $fixture[0]->getDeathDay());
        self::assertSame('Something New', $fixture[0]->getDeathMonth());
        self::assertSame('Something New', $fixture[0]->getDeathYear());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Author();
        $fixture->setName('My Title');
        $fixture->setOpenlibraryKey('My Title');
        $fixture->setBirthDay('My Title');
        $fixture->setBirthMonth('My Title');
        $fixture->setBirthYear('My Title');
        $fixture->setDeathDay('My Title');
        $fixture->setDeathMonth('My Title');
        $fixture->setDeathYear('My Title');

        $this->repository->add($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/author/');
    }
}
